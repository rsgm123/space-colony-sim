let path = require('path');
let webpack = require('webpack');

module.exports = {
  entry: {
    client: path.resolve(__dirname, 'client/app.js'),
    host: path.resolve(__dirname, 'game-engine/app.js'),
  },

  output: {
    path: __dirname,
    filename: '[name].js'
  },

  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        exclude: /node_modules|venv|server/,
      },

      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules|venv|server/,
      },

      {
        test: /\.(scss|css)$/,
        loaders: ["style-loader", "css-loader?sourceMap", "sass-loader?sourceMap"],
        exclude: /node_modules|venv|server/,
      },

      {
        test: /\.(png|jpg|gif|svg)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]'
        }
      },
    ]
  },

  resolve: {
    alias: {
      'vue$': 'vue/dist/vue.esm.js'
    }
  },

  devServer: {
    proxy: {
      '/server': 'http://localhost:5000/',
    },
    historyApiFallback: true,
    noInfo: true
  },

  performance: {
    hints: false
  },

  devtool: "source-map",
};

module.exports.plugins = [
  new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
];

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map';
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"'
      }
    }),
    new webpack.optimize.UglifyJsPlugin({
      sourceMap: true,
      compress: {
        warnings: false
      }
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
}
