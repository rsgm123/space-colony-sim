import moment from 'moment';

const levels = {
  trace: 0,
  debug: 1,
  info: 2,
  warn: 3,
  error: 4,
};
const levelNames = {
  0: 'trace',
  1: 'debug',
  2: 'info',
  3: 'warn',
  4: 'error',
};

const defaultLevel = 'trace';
let outputLevel = levels[defaultLevel];

const environment = 'client';

const css = {
  level: {
    0: 'color: #40a844; font-weight: bold;',
    1: 'color: #3b8fdb; font-weight: bold;',
    2: 'color: #eeeeee; font-weight: bold;',
    3: 'color: #e3c322; font-weight: bold;',
    4: 'color: #bd5036; font-weight: bold;',
  },

  time: '',
  location: 'color: #cbe83a;',
  log: 'color: #dee1ff;',
};


function output(level, location, log) {
  let display = [
    '%c' + moment().format('YYYY-MM-DD mm:ss:SS'),
    '%c' + levelNames[level],
    '%c' + environment + '/' + location,
  ];

  if (level >= outputLevel) {
    return [
      display.join(' - ') + '\n%c%O',
      css.level[level] + css.time,
      css.level[level],
      css.location,
      css.log,
      log,
    ]
  }
}


export let log = {
  trace(location, log) {
    console.log(...output(levels.trace, location, log));
  },

  debug(location, log) {
    console.log(...output(levels.debug, location, log));
  },

  info(location, log) {
    console.log(...output(levels.info, location, log));
  },

  warn(location, log) {
    console.warn(...output(levels.warn, location, log));
  },

  error(location, log) {
    console.error(...output(levels.error, location, log));
  },
};
