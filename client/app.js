import './sass/main.scss'

import Vue from 'vue'
import VueRouter from 'vue-router'
import axios from 'axios'
import VueAxios from 'vue-axios'

import net from './app/plugins/lobby.js'
import events from './app/plugins/events.js'
import game from './app/plugins/game.js'
import log from './app/plugins/log.js'
import store from './app/plugins/store.js'
import util from './app/plugins/util.js'

import navbar from './app/components/navbar.vue'

import routes from './routes.js'

Vue.config.devtools = true;

// https://github.com/alexei/sprintf.js
String.format = require("sprintf-js").sprintf; // fuck it, this is the nicest way with webpack. hope they never add format to the ecma specs


// https://github.com/vue-comps


Vue.use(VueRouter);
Vue.use(VueAxios, axios);

// our plugins
Vue.use(events);
Vue.use(net);
Vue.use(game);
Vue.use(log);
Vue.use(store);
Vue.use(util);


Vue.component('navbar', navbar);


Vue.filter('capitalize', function (value) {
  if (!value) {
    return '';
  }

  let words = value.toString().split(' ').map((s) => {
    return s.charAt(0).toUpperCase() + s.slice(1)
  });
  return words.join(' ');
});


const router = new VueRouter({
  routes
});


const app = new Vue({
  router,
}).$mount('#app');


// temp test data
Vue.store.merge({
  systems: [
    {id: 0, name: 'start', position: {x: 300, y: 500}},
    {id: 1, name: 'new system', position: {x: 100, y: 300}},
    {id: 2, name: 'test', position: {x: 300, y: 400}},
  ],
  connections: [
    {id: 0, systems: [0, 1]},
  ],
  celestials: [
    {
      id: 0,
      type: 'star',
      size: 'small',
      position: {system: 0, x: 500, y: 200},
      graphics: {
        icon: 'star',
      },
      stats: {color: 'hsl(55, 40%, 70%)', orbit: false},
      resources: {}
    },
    {
      id: 1,
      type: 'planet',
      size: 'small',
      position: {system: 0, x: 650, y: 400},
      parent: {id: 0, x: 500, y: 200},
      graphics: {
        icon: 'planet',
      },
      stats: {color: '#38814C', atmosphere: true, orbit: true},
      resources: {}
    },
    {
      id: 2,
      type: 'moon',
      size: 'small',
      position: {system: 0, x: 500, y: 400},
      parent: {id: 1, x: 650, y: 400},
      graphics: {
        icon: 'moon',
      },
      stats: {color: '#818181', orbit: true},
      resources: {}
    },
    {
      id: 3,
      type: 'planet',
      size: 'small',
      position: {system: 0, x: 200, y: 200},
      parent: {id: 0, x: 500, y: 200},
      graphics: {
        icon: 'planet',
      },
      stats: {color: '#1B56C1', orbit: true},
      resources: {}
    },
    {
      id: 4,
      type: 'planet',
      size: 'small',
      position: {system: 0, x: 1400, y: 600},
      parent: {id: 0, x: 500, y: 200},
      graphics: {
        icon: 'planet',
      },
      stats: {color: '#814B32', orbit: true},
      resources: {}
    },
    {
      id: 5,
      type: 'planet',
      size: 'small',
      position: {system: 0, x: 200, y: 800},
      parent: {id: 0, x: 500, y: 200},
      graphics: {
        icon: 'planet',
      },
      stats: {color: '#6296C1', orbit: true},
      resources: {}
    },
    // {
    //   id: 6,
    //   type: 'wormhole',
    //   size: 'small',
    //   position: {system: 0, x: 100, y: 0},
    //   parent: {id: 0, x: 500, y: 200},
    //   graphics: {
    //     icon: 'planet',
    //   },
    //   stats: {connection: 1, orbit: false},
    //   resources: {}
    // },
  ],
  entities: [
    {
      id: 0,
      name: 'home',
      player: 0,
      position: {
        system: 0,
        x: 670,
        y: 480,
      },
      velocity: {
        x: 15,
        y: -5,
      },
      graphics: {
        icon: 'ship',
      },
      hp: {
        shield: 1000, // once shield is down, hull and subsystems can get hit
        hull: 1000,
        components: {}
      },
      cargo: [],
      blueprint: 0,
    },
  ],
  blueprints: [
    {
      id: 0,
      name: 'scout blueprint',
      basetime: 6000,
      inputs: {},
      stats: {
        volume: 100000,
        shield: 1000,
        hull: 1000,

        speed: 3000,

        activeSensors: 50000,
        passiveSensors: 500000,

        graphics: {
          iconComponent: 'shipSmall',
          zIndex: 10,
        },
      },
    },
  ],
  // players: [
  //   {id: 0, name: 'player 0', color: '#ffffff', relation: 20},
  //   {id: 1, name: 'player 1', color: '#ffffff', relation: 0},
  //   {id: 2, name: 'player 2', color: '#ffffff', relation: 0},
  // ],
});
