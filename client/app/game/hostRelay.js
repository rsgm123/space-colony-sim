// todo: remove this once webrtc works in webworkers
import hostHandler from "./hostHandler";
import {log} from "../../logger";

let hostWorker = undefined;

let playerCount = undefined;
const players = {};


export const relayEvents = {
  hostGame: 'host_game',
  rtc: 'rtc',
  start: 'start',
};


function setChannel(player, channel) {
  if (!players[player] || !players[player].peer) {
    log.error('hostRelay', 'player does not exist');
    return
  } else if (players[player].channel) {
    log.error('hostRelay', 'player channel already exists');
    return
  }

  players[player].channel = channel;
  log.debug('hostRelay', 'player channel created');


  channel.onopen = function (event) {
    log.debug('hostRelay', ['peer chanel opened', event]);

    if (players.length === playerCount && players.values.reduce((a, b) => {
          return a && b.channel && b.channel.readyState === 'open'
        }, true)) {
      hostWorker.postMessage({event: 'playersConnected', data: {}});
    }
  };

  channel.onerror = function (event) {
    log.error('hostRelay', ['onerror', event]);
  };

  channel.onclose = function (event) {
    log.debug('hostRelay', ['onclose', event]);
  };

  channel.onmessage = function (event) {
    log.debug('hostRelay : channel onMessage', event);
    hostWorker.postMessage(event);
  };
}


function addPeer(player) {
  let peer = new RTCPeerConnection();
  players[player] = {peer: peer};

  let channel = peer.createDataChannel("data");
  setChannel(player, channel);

  peer.onicecandidate = function (event) {
    if (event.candidate) {
      log.trace('hostRelay : onicecandidate', event);
      // let candidate = JSON.stringify(event.candidate);
      hostWorker.postMessage({
        event: relayEvents.rtc,
        data: {rtcData: event.candidate.toJSON(), to: player}
      });
    }
  };

  peer.createOffer().then(function (description) {
    log.trace('hostRelay : createOffer', description);
    return peer.setLocalDescription(description)
  }).then(function () {
    log.trace('hostRelay : send offer', peer.localDescription);
    hostWorker.postMessage({
      event: relayEvents.rtc,
      data: {rtcData: peer.localDescription.toJSON(), to: player}
    });
  }).catch(function (e) {
    log.error('hostRelay', e)
  });
}

function respondToPeer(rtcData, player) {
  let peer = players[player].peer;

  if (rtcData.sdp) {
    let desc = new RTCSessionDescription(rtcData);
    peer.setRemoteDescription(desc).then(function () {
      // if (rtcData.type === 'offer') {
      //   log.trace('hostRelay', desc);
      //   peer.createAnswer().then(function (answerDescription) {
      //     peer.setLocalDescription(answerDescription).then(function () {
      //       socketSend({
      //         event: relayEvents.rtc,
      //         data: {rtcData: answerDescription.toJSON(), to: player}
      //       });
      //     });
      //   });
      // }
    });

  } else if (rtcData.candidate) {
    log.trace('hostRelay : addIceCandidate', rtcData);
    peer.addIceCandidate(new RTCIceCandidate(rtcData)); // post to server
  }
}


const handler = {
  players(data) {
    playerCount = data.players.length;
    data.players.forEach((player) => {
      addPeer(player.id)
    });
  },

  rtc(data) {
    respondToPeer(data.rtcData, data.from)
  },

  relaySetup(data) {
    hostWorker.onmessage = (event) => {
      players.values((player) => {
        player.channel.send(event.data);
      });
    };
  }
};


export default {
  // todo: remove this once webrtc works in webworkers
  tempStartHost(game) {
    hostWorker = new Worker('host.js'); // will this get garbage collected?

    hostWorker.onmessage = (event) => {
      log.info('hostRelay : onmessage', event);
      handler[event.data.event](event.data.data);
    };

    hostWorker.postMessage({game: game});
  }
}
