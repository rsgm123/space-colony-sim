import {events, eventBus} from '../plugins/events'
import hostHandler from './hostHandler'
import {log} from "../../logger";


let socket = undefined;


export const lobbyEvents = {
  createGame: 'create_game',
  joinGame: 'join_game',
  leaveGame: 'leave_game',

  chat: 'chat',
  parameters: 'parameters',
  updatePlayer: 'update_player',
  updateSettings: 'update_settings',

  rtc: 'rtc',
};


const handlers = {
  gameCreated(data) {
    eventBus.$emit(events.lobby.created, data);
  },
  joinedGame(data) {
    eventBus.$emit(events.lobby.joined, data);
  },
  playerJoined(data) {
    eventBus.$emit(events.lobby.playerJoined, data);
  },
  playerLeft(data) {
    eventBus.$emit(events.lobby.playerLeft, data);
  },
  playerUpdate(data) {
    eventBus.$emit(events.lobby.playerUpdated, data);
  },
  settingsUpdate(data) {
    eventBus.$emit(events.lobby.settingsUpdated, data);
  },
  parametersUpdate(data) {
    eventBus.$emit(events.lobby.settingsUpdated, data);
  },
  rtc(data) {
    respondToPeer(data.rtcData, data.to);
  },
  chat(data) {
    eventBus.$emit(events.chat, data);
  },
};


function setupLobby() {
  if (socket) {
    return
  }

  socket = new WebSocket('ws:localhost:5000/server');

  // emit connect event to send all messages waiting on a connection
  socket.onopen = function (event) {
    eventBus.$emit(events.lobby.connect);
  };

  // setup socket message handling
  socket.onmessage = function (event) {
    let message = JSON.parse(event.data);
    let data = message.data;
    log.debug('socketHandler', message);

    handlers[message.event](data);
  };

  eventBus.$once(events.lobby.startGame, leaveLobby);
}


function leaveLobby(message) {
  socket.close(1000, message)
}


function socketSend(data) {
  if (!socket || socket.readyState !== 1) {
    log.error('lobbyHandler', 'message trying to send before socket is open');

  } else {
    log.debug('lobbyHandler', data);
    socket.send(JSON.stringify(data))
  }
}


function respondToPeer(rtcData, player) {
  let peer = hostHandler.getHost();

  if (!peer) {
    peer = new RTCPeerConnection();
    hostHandler.setPeer(peer);

    peer.ondatachannel = function (e) {
      log.debug('lobbyHandler : ondatachannel', ['new peer', e]);
      hostHandler.setChannel(e.channel);
    };

    peer.onicecandidate = function (event) {
      if (event.candidate) {
        log.trace('lobbyHandler : onicecandidate', event);
        // let candidate = JSON.stringify(event.candidate);
        socketSend({
          event: lobbyEvents.rtc,
          data: {rtcData: event.candidate.toJSON(), from: player}
        });
      }
    };
  }

  if (rtcData.sdp) {
    let desc = rtcData;
    peer.setRemoteDescription(desc).then(function () {
      log.trace('lobbyHandler : setRemoteDescription', desc);
      return peer.createAnswer()
    }).then(function (answerDescription) {
      return peer.setLocalDescription(answerDescription)
    }).then(function () {
      log.debug('lobbyHandler : set remote desc',peer.localDescription);
      socketSend({
        event: lobbyEvents.rtc,
        data: {rtcData: peer.localDescription.toJSON(), from: player}
      });
    }).catch(function() {
      // errors
    });

  } else if (rtcData.candidate) {
    log.trace('setRemoteDescription : addIceCandidate', rtcData);
    peer.addIceCandidate(new RTCIceCandidate(rtcData)); // post to server
  }
}


export default {
  setupLobby,
  leaveLobby,
  socketSend,
}