import {net} from '../plugins/lobby'
import {events, eventBus} from '../plugins/events'
import {log} from '../../logger'


let peer = undefined;
let channel = undefined;


export const hostEvents = {
  chat: 'chat'
};


export const handlers = {
  chat(data) {
    eventBus.$emit(events.chat, data);
  },

  gameStart() {
    eventBus.$emit(events.lobby.startGame);
  },
};

export default {
  getHost() {
    return peer
  },

  setPeer(p) {
    if (peer || channel) {
      log.error('hostHandler', 'host already set');
      return
    }

    peer = p;
  },

  setChannel(c) {
    if (channel) {
      log.error('net', 'player channel already exists');
      return
    }

    channel = c;

    channel.onerror = function (event) {
      log.error('hostHandler', ['onerror', event]);
    };

    channel.onclose = function (event) {
      log.debug('hostHandler', ['onclose', event]);
    };

    channel.onmessage = function (event) {
      let message = JSON.parse(event.data);
      let data = message.data;
      log.debug('socketHandler', message);

      handlers[message.event](data);
    };

    channel.onopen = function (event) {
      log.debug('hostHandler', ['peer chanel opened', event]);

      eventBus.$emit(events.lobby.hostConnected);
    };
  },

  send() {
    if (!socket || socket.readyState !== 1) {
      log.error('hostHandler', 'message trying to send before socket is open');

    } else {
      log.debug('lobbyHandler', data);
      channel.send(JSON.stringify(data))
    }
  },
}
