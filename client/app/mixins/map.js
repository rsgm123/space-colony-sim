let svg;

export default {
  data() {
    return {
      map: { // map namespace
        zoom: 0, // exponent of the scaling step, see zoom watcher
        zoomScale: 1.25,
        zoomBounds: 8,
        zoomPos: { //
          x: 0,
          y: 0,
        },

        isPanning: false,
        
        matrix: { // current transformation matrix
          a: 1,
          b: 0,
          c: 0,
          d: 1,
          e: 0,
          f: 0,
        },
      }
    }
  },

  computed: {
    mapTransform() {
      return String.format('matrix(%f, %f, %f, %f, %f, %f)', this.map.matrix.a, this.map.matrix.b, this.map.matrix.c, this.map.matrix.d, this.map.matrix.e, this.map.matrix.f);
    },

    scale() {
      return this.map.matrix.a;
    },

    textScale() {
      return 1 / this.map.matrix.a;
    },
  },

  methods: {
    addSvgListeners(svgElem) {
      svg = svgElem;

      // scrolling
      svg.addEventListener("mousewheel", this.mapScroll, false); // chrome, others
      svg.addEventListener("DOMMouseScroll", this.mapScroll, false); // firefox

      // panning
      svg.onmousedown = this.mapStartPan;
      svg.onmouseup = this.mapCancelPan;
      svg.onmouseout = this.mapCancelPan;
      svg.onmousemove = this.mapDrag;
    },

    mapScroll(e) {
      let zoomIn = e.detail > 0;
      let bounds = Math.pow(this.map.zoomScale, this.map.zoomBounds);
      if ((this.scale > 1 / bounds || zoomIn) && (this.scale < bounds || !zoomIn)) {
        this.map.zoom += zoomIn ? 1 : -1;

        let r = svg.getBoundingClientRect();
        let point = svg.createSVGPoint();
        point.x = e.clientX - r.x; // adjust point for svg location
        point.y = e.clientY - r.y;

        let oldMatrix = this.mapCopyMatrix();

        let relPoint = point.matrixTransform(oldMatrix.inverse());
        let modifier = svg.createSVGMatrix().translate(relPoint.x, relPoint.y).scale(zoomIn ? this.map.zoomScale : 1 / this.map.zoomScale).translate(-relPoint.x, -relPoint.y);
        let m = oldMatrix.multiply(modifier);

        this.map.matrix = {a: m.a, b: m.b, c: m.c, d: m.d, e: m.e, f: m.f};
      }
    },

    mapStartPan(e) {
      this.map.isPanning = true;
    },

    mapCancelPan(e) {
      if (e.type === 'mouseup') {
        this.map.isPanning = false;
      } else if (this.map.isPanning) { // check to see if we hit children nodes when mouseout occurs
        let elem = e.toElement || e.relatedTarget;
        while (elem) { // this won't slow anything down since it shouldn't run unless there is lag while dragging
          if (elem === svg) {
            return; // ignore hitting children
          }
          elem = elem.parentNode;
        }
        this.map.isPanning = false;
      }
    },

    mapDrag(e) {
      if (this.map.isPanning) { // todo: either have a button to center or put bounds in
        this.map.matrix.e += e.movementX;
        this.map.matrix.f += e.movementY;
      }
    },

    mapCopyMatrix(){
      let old = svg.createSVGMatrix();
      old.a = this.map.matrix.a;
      old.b = this.map.matrix.b;
      old.c = this.map.matrix.c;
      old.d = this.map.matrix.d;
      old.e = this.map.matrix.e;
      old.f = this.map.matrix.f;
      return old;
    }
  }
}
