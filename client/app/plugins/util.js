let idCount = 0;

export default {
  install(Vue) {
    // Vue.store = function () {
    // };

    Vue.prototype.$util = {
      elementId(options) {
        return 'e-' + (idCount++);
      }
    }
  }
}
