/**
 * Plugin for game actions to send to the host.
 */
import hostHandler, {hostEvents} from "../game/hostHandler";


const game = {
  sendChat(text) {
    hostHandler.send({event: hostEvents.chat, data: {text: text}})
  },
};


export default {
  install: function (Vue) {
    Vue.game = game;

    // Vue.mixin({
    //   created: function () {
    //   }
    // });

    Vue.prototype.$game = game;
  }
}
