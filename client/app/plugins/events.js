export let eventBus;

export const events = {
  lobby: {
    connect: 'lobby/onScoketConnect',

    created: 'lobby/created',
    joined: 'lobby/joined',

    playerJoined: 'lobby/playerJoined',
    playerLeft: 'lobby/playerLeft',
    playerUpdated: 'lobby/playerUpdated',
    settingsUpdated: 'lobby/settingsUpdated',

    hostConnected: 'lobby/hostConnected',
    startGame: 'lobby/startGame',
  },

  game: {
    created: 'game/created', // connected to host, host is waiting for and loading initial state from server
    initialized: 'game/initialized', // game initialized, start loading game from host
    started: 'game/started', // begin game play
  },

  net: {
  },

  store: {
    update: 'store/storeUpdate', // game events
  },

  map: {
    deselect: 'map/deselect', // deselect entities
  },

  chat: 'chat' // generic event to push messages to chat
};


export default {
  install: function (Vue) {
    eventBus = new Vue();

    Vue.events = events;
    Vue.prototype.$events = events;

    Vue.eventBus = eventBus;
    Vue.prototype.$eventBus = eventBus;
  }
}
