import socket, {lobbyEvents} from '../game/lobbyHandler'
import {events, eventBus} from "./events";
import hostHandler from "../game/hostHandler";
import hostRelay from "../game/hostRelay";


let playerId = undefined;

const lobby = {
  createGame(isPublic, name, password) {
    socket.setupLobby(); // sets up socket handlers and connects to peers

    socket.socketSend({
      event: lobbyEvents.createGame,
      data: {
        'public': isPublic,
        name,
        password
      }
    });
  },

  joinGameLobby(game) {
    socket.setupLobby();

    eventBus.$once(events.lobby.joined, (data) => {
      playerId = data.player
    });
    socket.socketSend({event: lobbyEvents.joinGame, data: {game: game}});
  },

  leaveGameLobby() {
    socket.socketSend({event: lobbyEvents.leaveGame, data: {}});
    socket.leaveLobby();
  },

  sendChat(text) {
    socket.socketSend({event: lobbyEvents.chat, data: {text: text}})
  },

  updatePlayer(player) {
    socket.socketSend({event: lobbyEvents.updatePlayer, data: player})
  },

  updateSettings(settings) {
    socket.socketSend({event: lobbyEvents.updateSettings, data: settings})
  },

  host(game) {
    hostRelay.tempStartHost(game)
  }
};


/*
 * functions for interacting with the host
 */
export default {
  install: function (Vue) {
    Vue.lobby = lobby;
    Vue.prototype.$lobby = lobby;
  }
}
