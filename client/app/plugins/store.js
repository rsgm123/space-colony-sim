let taffy = require('taffydb').taffy;

let tables;


/**
 * Maps query results arrays to an object of with resultId -> result
 *
 * @param results query result array
 */
function mapIds(results) {
  let resultObject = {};
  results.forEach((row) => {
    resultObject[row.id] = row;
  });

  return resultObject;
}


export default {
  install(Vue) {
    tables = {
      players: taffy(),

      systems: taffy(),
      connections: taffy(),
      celestials: taffy(),

      entities: taffy(),

      items: taffy(),
      blueprints: taffy(),
      builds: taffy(),
    };

    Vue.store = {
      /**
       * Merge all table updates.
       * This will replace existing objects in the db. Use Vue.store.update to update individual columns.
       *
       * @param inserts a mapping from table names an array of objects to be inserted
       */
      merge(inserts){
        for (var key in inserts) {
          if (!tables.hasOwnProperty(key)) {
            console.error("no table named " + key + ", this breaks things");
            return;
          }

          tables[key].merge(inserts[key]);
        }

        Vue.eventBus.$emit(Vue.events.store.update);
      },

      /**
       * Updates objects based on changed properties.
       * Only provide properties that you wish to change.
       *
       * @param updates a mapping from table names to an array of objects to be updated
       */
      update(updates){
        for (let key in updates) {
          if (!tables.hasOwnProperty(key)) {
            console.error("no table named " + key + ", this breaks things");
            return;
          }

          updates[key].forEach((value) => {
            tables[key]({id: value.id}).update(value);
          });
        }

        Vue.eventBus.$emit(Vue.events.store.update);
      },
    };

    // Vue.mixin({
    //   created() { // runs in every component
    //   },
    //
    //   methods: {},
    // });

    Vue.prototype.$store = { // component methods, nothing here should mutate state

      /*
       * Table getters
       * Use these to get any data to display from a component. These cannot mutate the state of the game.
       * These take an optional query argument to filter results, descripbed here: www.taffydb.com/writingqueries
       *
       * todo: if memory gets bad look into restricting object columns
       */
      get: {
        players(query) {
          return mapIds(tables.players(query).get())
        },

        systems(query) {
          return mapIds(tables.systems(query).get())
        },

        connections(query) {
          return mapIds(tables.connections(query).get())
        },

        celestials(query) {
          return mapIds(tables.celestials(query).get())
        },

        entities(query) {
          return mapIds(tables.entities(query).get())
        },

        items(query) {
          return mapIds(tables.items(query).get())
        },

        blueprints(query) {
          return mapIds(tables.blueprints(query).get())
        },

        builds(query) {
          return mapIds(tables.builds(query).get())
        },
      },
    }
  }
}
