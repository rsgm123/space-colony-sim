import {log} from '../../logger'

export default {
  install(Vue) {
    Vue.log = log;
    Vue.prototype.$log = log;
  }
}
