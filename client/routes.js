import home from './app/home.vue'
import lobby from './app/lobby.vue'
import createGame from './app/createGame.vue'
import gameLobby from './app/gameLobby.vue'
import game from './app/game.vue'

import starMap from './app/components/starMap.vue'
import systemMap from './app/components/systemMap.vue'

export default [
  {
    path: '/',
    name: 'home',
    component: home,
  },

  {
    path: '/lobby',
    name: 'lobby',
    component: lobby,
  },

  {
    path: '/lobby/create',
    name: 'create game',
    component: createGame,
  },

  {
    path: '/lobby/:gameId',
    name: 'game lobby',
    component: gameLobby,
    props: true,
  },

  {
    path: '/game',
    component: game,
    children: [
      {
        path: '/',
        name: 'star map',
        component: starMap,
      },

      {
        path: 'system-map/:systemId',
        name: 'system map',
        component: systemMap,
        props: true,
      },
    ]
  },
]
