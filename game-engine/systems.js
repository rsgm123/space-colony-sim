/**
 * Object containing the systems for the game loop.
 * Each function takes only a single object.
 *
 * Do not recurse.
 * Do not call a system from within a system.
 *
 * Note that the order below is arbitrary
 */

function movement (entity) {
  if (entity.position && entity.position.x && entity.velocity && entity.velocity.x) {

    if (entity.destination) { // check if destination x and/or y is within 1 tick of movement 
      if (entity.destination.x === entity.position.x && entity.destination.y === entity.position.y) {
        operations.taskComplete(entity); // do this first so the entity will be still for a tick
      }

      if (entity.destination.x - entity.position.x <= entity.velocity.x) {
        entity.veocity.x = 0;
        entity.position.x = entity.destination.x
      }  
      
      if (entity.destination.y - entity.position.y <= entity.velocity.y) {
        entity.velocity.y = 0;
        entity.position.y = entity.destination.y
      }  
    } else {
      entity.position.x += entity.velocity.x;
      entity.position.y += entity.velocity.y;
    }
    return entity.id
  }
}

function resourceNeeds () {
  // fuel requirements, colony needs, etc.
}

function resourceMining (entity) {
  if (entity.mining && entity.mining.active) {
    operations.mining(entity, entity.mining.target, );
    return entity.id
  }
}

function weaponCooldown (entity) {
  if (entity.cooldown !== undefined && entity.target !== undefined) {
    if (entity.cooldown > 0) { // weapon cooldown
      entity.cooldown -= 1;
    } else if (entity.target) { // fire weapon
      operations.randomOrder(operations.fireWeapon, [entity.target, entity.ship, entity.info]);
    }
  }
}


// ordered list of systems
export default [
  movement,
  resourceMining,
  resourceNeeds,
  weaponCooldown,
]


export function buildSystem (build) {
  if (build.ticksLeft > 0) {
    build.ticksLeft -= 1;
  } else {
    operations.finishBuild(build);
  }
}
