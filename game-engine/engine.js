// import PriorityQueue from 'js-priority-queue'
import lobbyHandler from "./lobbyHandler"

// https://github.com/adamhooper/js-priority-queue

let taffy = require('taffydb').taffy;

let currentTick = 0; // current game tick

let tableCounts = { // counts to be used as IDs
  players: 0,
  systems: 0,
  connections: 0,
  celestials: 0,
  entities: 0,
  items: 0,
  blueprints: 0,
  builds: 0,
};

let tables = [
  'players',

  'systems',
  'connections',
  'celestials',

  'entities',

  'items',
  'blueprints',
  'builds',
];

let database = undefined;


/**
 * Initialize database before initializing game state
 */
function initDB() {
  database = {
    players: taffy(),

    systems: taffy(),
    connections: taffy(),
    celestials: taffy(),

    entities: taffy(),

    items: taffy(),
    blueprints: taffy(),
    builds: taffy(),
  }

  // let openRequest = indexedDB.open("spacecolonysim.hostDatabase", {version: 1, storage: "persistent"});
  //
  // openRequest.onsuccess = (event) => {
  //   database = openRequest.result;
  //
  //
  //   initState()
  // };
  //
  // openRequest.onerror = (event) => {
  //   console.error(event)
  // };
}


function initState() {

}

function loop() {
  while (running) {
    tick(currentTick);

    // send updates to players


    currentTick++;
  }
}

function tick(tick) {
  tables.entities.list().forEach((entity) => {
    gameSystems.forEach((system) => {
      system(entity)
    })
  });

  tables.builds.list().forEach((build) => {
    buildSystem(build)
  });
}


export default {
  setupEngine() {
    initDB();
  },

  initState,

  startEngine() {
    loop()
  },
};
