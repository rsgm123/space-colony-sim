import {log} from './logger'
import lobbyHandler from "./lobbyHandler";

const players = {};


const handlers = {
  chat(data) {
  },
};


function onRelayMessage(event) {
  log.info('net', 'onmessage 3');

  let message = JSON.parse(event.data);
  let data = message.data;
  log.debug('socketHandler', message);

  handlers[message.event](data);
}


export default {
  // getPlayer() {
  //   return players[player]
  // },
  //
  // addPlayer(player, peer) {
  //   if (players[player]) {
  //     log.error('net', 'player already connected');
  //     return // maybe let these go? we might be doing this for a reason
  //   }
  //
  //   players[player] = {
  //     peer: peer
  //   };
  // },
  //
  // setChannel(player, channel) {
  //   if (!players[player] || !players[player].peer) {
  //     log.error('net', 'player does not exist');
  //     return
  //   } else if (players[player].channel) {
  //     log.error('net', 'player channel already exists');
  //     return
  //   }
  //
  //   players[player].channel = channel;
  //
  //   channel.onopen = function (event) {
  //     log.debug('net', ['peer chanel opened', event]);
  //   };
  //
  //   channel.onerror = function (event) {
  //     log.error('net', ['onerror', event]);
  //   };
  //
  //   channel.onclose = function (event) {
  //     log.debug('net', ['onclose', event]);
  //   };
  //
  //   channel.onmessage = function (event) {
  //     let message = JSON.parse(event.data);
  //     let data = message.data;
  //     log.debug('socketHandler', message);
  //
  //     handlers[message.event](data);
  //   };
  // },

  startNetRelay() {
    onmessage = function (event) {
      log.info('net : onmessage 2', event);

      log.debug('net : on worker message', [event, event.data]);
      if (event === 'playersConnected') {
        onmessage = onRelayMessage;
      } else {
        lobbyHandler.socketSend(event.data)
      }
    };
  },
}
