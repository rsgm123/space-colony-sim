import lobbyHandler from "./lobbyHandler";
import {log} from "./logger";

// setup utilities engine

// https://github.com/alexei/sprintf.js
String.format = require("sprintf-js").sprintf;


// start game
onmessage = function (e) {
// connect to players when gameId is recieved
  log.info('app : onmessage', e);
  lobbyHandler.setupLobby(e.data.game);
};
