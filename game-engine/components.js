/**
 * Object with component name constants for each type of stored objects.
 * Some object types may share components.
 * Add them here after using new components.
 *
 * Note that these are only component names. Component data is not defined, so be careful of mistakes.
 */
export default {
  // players
  name: 'name',
  relations: 'relations',
  graphics: 'graphics',
  ai: 'ai',

  // systems
  location: 'location',


  // connections
  systems: 'connections',


  // celestials
  star: 'star',
  planet: 'planet',
  stargate: 'stargate',
  wormhole: 'wormhole',

  resources: 'resources',
  atmosphere: 'atmosphere',
  outposts: 'outposts',


  // entity
  player: 'player',

  health: 'health',
  stats: 'stats',

  cargo: 'cargo',
  hangar: 'hangar',
  builds: 'builds',

  position: 'position',
  movement: 'movement',


  // items
  industry: 'industry',


  // blueprints


  // builds
  building: 'building',
  finished: 'finished',
  completed: 'completed',
}
