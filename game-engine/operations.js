
function taskComplete(entity) {
  let lastTask = entity.task; // todo: log/display this
  let task = entity.taskQueue.pop;
  entity.task = task;

  if (task.destination) {
    entity.destination = task.destination;

    let dist = Math.sqrt((entity.destination.x * entity.destination.x - entity.position.x * entity.position.x) + (entity.destination.y * entity.destination.y - entity.position.y * entity.position.y))
    let xDist = Math.sqrt(entity.destination.x * entity.destination.x - entity.position.x * entity.position.x);
    let yDist = Math.sqrt(entity.destination.y * entity.destination.y - entity.position.y * entity.position.y);

    let xScalar = xDist/dist;
    let yScalar = yDist/dist;

    entity.velocity = {x: xScalar * entity.maxSpeed, y: yScalar * entity.maxSpeed}
  }
}

/**
 * Global utilities that require access to the game engine.
 * 
 */
export operations {
  randomOrder(operationName, args) {
    if (operationName === constants.fireWeapon) {
      operationQueue[operationName].push({fn: functions.fireWeapon, args: args});
    }
  },

  runOperations() {
    for (operation in constants.order) {
      for (call in operatonQueue[operation]) {
        call.fn.apply(call.args);
      }
    }
  },

  damage(target, cause, info) {
    if (info) {
      if (info.targetComponent) {
      }
    }
  },
}
