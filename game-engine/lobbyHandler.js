import net from './net'
import {log} from "./logger";
import engine from "./engine";

let socket = undefined;

export const lobbyEvents = {
  hostGame: 'host_game',
  rtc: 'rtc',
  start: 'start',
};


const lobbyHandlers = {
  players(data) {
    postMessage({event: 'players', data: {players: data.players}});
    // data.players.forEach((player) => {addPeer(player.id)});
  },

  rtc(data) {
    // respondToPeer(data.rtcData, data.from);
    postMessage({event: 'rtc', data: data})
  },

  gameGenerated(data) {
    engine.initState(data);

    socket.close(1000, message)
  }
};


function setupLobby(game) {
  if (socket) {
    log.error('lobbyHandler', 'socket already exists');
    return
  }

  log.debug('lobbyHandler : setupLobby', 'socket opened');
  socket = new WebSocket('ws:localhost:5000/server');

  socket.onopen = function (m) {
    socketSend({event: lobbyEvents.hostGame, data: {game: game}})
  };

  // setup socket message handling
  socket.onmessage = function (m) {
    let message = JSON.parse(m.data);
    let data = message.data;
    log.debug('lobbyHandler', message);

    lobbyHandlers[message.event](data);
  };

  net.startNetRelay();
}


function socketSend(data) {
  if (!socket || socket.readyState !== 1) {
    log.error('lobbyHandler', 'message trying to send before socket is open');

  } else {
    log.debug('lobbyHandler : socketSend', data);
    socket.send(JSON.stringify(data))
  }
}


// function addPeer(player) {
//   let peer = new RTCPeerConnection();
//   net.setPeer(player, peer);
//
//   let channel = peer.createDataChannel("data");
//   net.setChannel(player, channel);
//
//   peer.onicecandidate = function (event) {
//     if (event.candidate) {
//       log.trace('lobbyHandler', event.candidate);
//       socketSend({
//         event: lobbyEvents.rtc,
//         data: {rtcData: event.candidate, to: player}
//       });
//     }
//   };
//
//   peer.createOffer().then(function (description) {
//     peer.setLocalDescription(description).then(function () {
//       log.trace('lobbyHandler', description);
//       socketSend({
//         event: lobbyEvents.rtc,
//         data: {rtcData: description.toJSON(), to: player}
//       })
//     });
//   }, function (e) {
//     log.error('lobbyHandler', e)
//   });
// }
//
//
// function respondToPeer(rtcData, player) {
//   let peer = net.getPlayer();
//
//   if (rtcData.sdp) {
//     let desc = new RTCSessionDescription(rtcData);
//     peer.setRemoteDescription(desc).then(function () {
//       // if (rtcData.type === 'offer') {
//       //   log.trace('lobbyHandler', desc);
//       //   peer.createAnswer().then(function (answerDescription) {
//       //     peer.setLocalDescription(answerDescription).then(function () {
//       //       socketSend({
//       //         event: lobbyEvents.rtc,
//       //         data: {rtcData: answerDescription.toJSON(), to: player}
//       //       });
//       //     });
//       //   });
//       // }
//     });
//
//   } else if (rtcData.candidate) {
//     log.trace('lobbyHandler', rtcData);
//     peer.addIceCandidate(new RTCIceCandidate(rtcData)); // post to server
//   }
// }


export default {
  setupLobby,
  socketSend,
}
