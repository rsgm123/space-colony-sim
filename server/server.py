from websockets import LobbyProtocol

if __name__ == '__main__':
    import asyncio

    from autobahn.asyncio.websocket import WebSocketServerFactory

    factory = WebSocketServerFactory()
    factory.protocol = LobbyProtocol

    loop = asyncio.get_event_loop()
    coro = loop.create_server(factory, 'localhost', 5000)
    server = loop.run_until_complete(coro)

    try:
        loop.run_forever()
    except KeyboardInterrupt:
        pass
    finally:
        server.close()
        loop.close()
