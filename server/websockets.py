import json

from autobahn.asyncio.websocket import WebSocketServerProtocol

# temporary game storage, replace this when using multiple server processes
games = {'count': 0}  # keep count in here, since the rest of the ids are integers


def get_player(player):
    """
    Creates a new player object without values that only the server uses.
    :param player: player object
    :return: new player object
    """
    return {
        'id': player['id'],
        'name': player['name'],
        'color': player['color'],
    }


class LobbyProtocol(WebSocketServerProtocol):
    game = None
    player = None

    def broadcast(self, message):
        for k, v in self.game['players'].items():
            v['socket'].send(message)

    def onConnect(self, request):
        print("Client connecting: {}".format(request.peer))

    def onOpen(self):
        print("WebSocket connection open.")

    def onMessage(self, payload, isBinary):
        if isBinary:
            print("Binary message received: {} bytes".format(len(payload)))
            return

        print("Text message received: {}".format(payload.decode('utf8')))

        message = json.loads(payload.decode('utf8'))
        if 'event' in message and message['event'] not in ['onOpen', 'onConnect', 'onMessage', '__call__']:
            response = self.__getattribute__(message['event'])(message['data'])

            if response:
                self.send(response)

    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {}".format(reason))
        self.leave_game(None)

    def send(self, message):
        self.sendMessage(json.dumps(message, ensure_ascii=False).encode('utf8'))

    def create_game(self, data):
        """
        expects: {
            public: boolean
            name: room name,
            password: password
        }
        """
        g = games['count'] = games['count'] + 1
        g = str(g)

        self.game = games[g] = {
            'id': g,  # game and room id
            'player_count': 0,
            'players': {},  # players in room
            'settings': {
                'difficulty': 0,
                'length': 1,
                'size': 1,
                'shape': 0,
                'resourceBalance': None,
            },
            'lobbySettings': data.get('settings', {}),  # settings not sent to players
        }

        return {'event': 'gameCreated', 'data': {'game': g}}

    def join_game(self, data):
        """
        expects: {
            game: gameId,
            password: (optional) password
        }
        """
        game_id = data['game']

        if not self.game:
            self.game = games[game_id]

        player_id = self.game['player_count'] = self.game['player_count'] + 1
        players = self.game['players']

        self.player = {
            'id': player_id,

            'name': player_id,
            'color': '#ffffff',
            'ready': False,

            'socket': self,
        }

        self.broadcast({
            'event': 'playerJoined',
            'data': {
                'player': get_player(self.player),
            }
        })

        players[player_id] = self.player

        return {
            'event': 'joinedGame',
            'data': {
                'game': game_id,
                'settings': self.game['settings'],
                'player': get_player(self.player),
                'players': {p: get_player(v) for p, v in players.items()},
            }
        }

    def leave_game(self, data, code=1000):
        """
        expects: {}
        """
        players = self.game['players']
        self.sendClose(code=code)
        del players[self.player['id']]

        self.broadcast({
            'event': 'playerLeft',
            'player': get_player(self.player),
        })

    def rtc(self, data):
        """
        simply relays rtc peer setup messages

        expects: {
            rtcData: rtcData,
            to: playerId,
        }
        """
        if 'from' in data:
            socket = self.game['host']['socket']
        else:
            socket = self.game['players'][data['to']]['socket']

        socket.send({
            'event': 'rtc',
            'data': data,
        })

    def chat(self, data):
        """
        expects: {
            user: playerId,
            message: messageText
        }
        """
        self.broadcast({
            'event': 'chat',
            'data': {
                'user': get_player(self.player),
                'text': data['text'],
            }
        })

    def update_player(self, data):
        """
        updates only the relevant values

        expects: {
            key: value
            ...
        }
        """
        self.player.update(data)
        self.broadcast({
            'event': 'playerUpdate',
            'data': get_player(self.player),
        })

    def update_settings(self, data):
        """
        updates only the relevant values

        expects: {
            key: value
            ...
        }
        """
        self.game['settings'].update(data)
        self.broadcast({
            'event': 'settingsUpdate',
            'data': self.game['settings'],
        })

    def host_game(self, data):
        """
        updates only the relevant values

        expects: {
            game: gameId
        }
        """
        self.game = games[data['game']]
        self.game['host'] = {
            'id': 'host',
            'socket': self,
        }

        self.send({
            'event': 'players',
            'data': {
                'players': [get_player(v) for _, v in self.game['players'].items()],
            }
        })

        # return generateGame(game['settings'])

    def start(self, data):
        """
        called by the host, sends finalized game information
        """
        pass
