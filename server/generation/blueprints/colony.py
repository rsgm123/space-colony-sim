colony = {
    'id': 0,
    'name': 'home',
    'type': 'outpost',
    'player': 0,

    'position': {
        'system': 0,
        'celestial': 1,
        'ground': False,
        'x': 670,
        'y': 440,
    },

    'cargo': [],
    'hangar': [],
    'builds': [],
    'population': 100,
    'blueprint': 5,
}
