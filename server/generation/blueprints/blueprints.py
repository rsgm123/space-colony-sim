blueprints = [{
    'id': 5,
    'name': 'home',
    'quantity': 1,

    # owner:

    'stats': {
        'mass': 1000000000,  # 1,000,000,000 kg
        'size': 1000000000,  # 1,000,000,000 m3
        'hp': 20000,

        'population': 500000,

        'totalReseach': 20,
    },

    'inputs': [
        {
            'id': 0,
            'name': 'Apartment',
            'quantity': 100,
        },
        {
            'id': 1,
            'name': 'Research Lab',
            'quantity': 20,
        },
    ],
}, {
    'id': 0,
    'name': 'Apartment',
    'quantity': 1,

    # owner:

    'stats': {
        'mass': 1000000000,
        'size': 1000000000,
        'hp': 20000,

        'population': 5000,
    },

    'inputs': {
        'wood': 2000,
        'steel': 5000,
        'concrete': 4000,
    },
}, {
    'id': 1,
    'name': 'Research Lab',
    'quantity': 1,

    # owner:

    'stats': {
        'mass': 1000000000,
        'size': 1000000000,
        'hp': 20000,

        'researchSpeed': 1,
    },

    'inputs': {
        'steel': 5000,
        'concrete': 8000,

        'simpleElectronics': 8000,
        'complexElectronics': 100,
    },
},
]
